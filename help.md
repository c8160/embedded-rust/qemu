qemu image, for virtualizing and emulating hardware.


Volumes:
PWD: Mapped to /project in the container, can contain files to expose to qemu
  (if needed). This is entirely optional and needn't be supplied when no files
  have to be shared with qemu.

Documentation:
For the underlying qemu project, see https://www.qemu.org/
For this container, see https://gitlab.com/c8160/embedded-rust/qemu

Requirements:
None

Configuration:
None

