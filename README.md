# qemu

qemu based on Alpine Linux.

> This image is tested for use with **podman** on **Linux** hosts with **x86_64** arch


## What is qemu?

qemu is a generic and open-source machine emulator and virtualizer.

See [the project website](https://www.qemu.org/).


## How to use this image

### Replacing a natively installed qemu

```bash
$ podman run --rm -it -v "$PWD:/project:z" registry.gitlab.com/c8160/embedded-rust/qemu --help
```

This will run qemu with `--help` as argument. If you don't need any file access
you can omit the `-v` argument.

<!--
### With a containerized GDB

If your debugger of choice is run from a container as well, you can skip the
port binding via `-p 3333:3333` and run both containers from within the same
container network.

```bash
$ podman run --rm -it (OTHER OPTIONS) --network openocd_net --name openocd openocd
$ podman run --rm -it (OTHER OPTIONS) --network openocd_net gdb
```

In this case you must make sure that the usual

```
(gdb) target remote :3333
```

from within GDB now becomes

```
(gdb) target remote openocd:3333
```

where *openocd* **must** match the name of the OpenOCD container you specified
with the `--name` flag.
-->


## Getting help

If you feel that something isn't working as expected, open an issue in the
Gitlab project for this container and describe what issue you are facing.

